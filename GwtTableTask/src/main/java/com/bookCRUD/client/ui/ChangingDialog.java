package com.bookCRUD.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.bookCRUD.client.EntryPointClass;
import com.bookCRUD.client.RemoteServiceIntf;
import com.bookCRUD.client.objects.Book;
import com.bookCRUD.client.objects.BookResult;
import com.bookCRUD.client.shared.Validator;

import java.util.Date;

public class ChangingDialog extends Dialog {
    interface ChangingDialogUiBinder extends UiBinder<HTMLPanel, ChangingDialog> { }
    private static ChangingDialogUiBinder ourUiBinder = GWT.create(ChangingDialogUiBinder.class);

    private Button changeButton = new Button();
    @UiField
    DialogBox dialogBox = new DialogBox();
    private int currentIndex;

    public ChangingDialog() {
        initWidget(ourUiBinder.createAndBindUi(this));
        initDialog(dialogBox, changeButton);
        initButton(changeButton);
    }

    @Override
    protected void initButton(Button button) {
        button.setText("Сохранить");
        button.addClickHandler(event -> {
            String title = getTitleInput().getText();
            String author = getAuthorInput().getText();
            Date date = getDateInput().getValue();
            Book book = new Book(title,author,date);
            if(!(Validator.isBookValid(book))){ return;}
            BookResult result = new BookResult(currentIndex,title,author,date);
            RemoteServiceIntf.App.getInstance().changeBook(result, new AsyncCallback<BookResult>() {
                @Override
                public void onFailure(Throwable caught) {
                    Window.alert(caught.getMessage());
                }

                @Override
                public void onSuccess(BookResult result) {
                    EntryPointClass.getProvider().getList().set(currentIndex,result);
                    dialogBox.hide();
                }
            });
        });
    }

    @Override
    protected void show() {
        dialogBox.center();
        dialogBox.show();
    }

    protected void show(BookResult result) {
        currentIndex = result.getId();
        getTitleInput().setText(result.getTitle());
        getAuthorInput().setText(result.getAuthor());
        getDateInput().setValue(result.getDate());
        dialogBox.center();
        dialogBox.show();
    }
}