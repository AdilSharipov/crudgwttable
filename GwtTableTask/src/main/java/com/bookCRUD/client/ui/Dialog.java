package com.bookCRUD.client.ui;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DefaultDateTimeFormatInfo;
import com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_en;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;

public abstract class Dialog extends Composite {
    private   Button cancelButton = new Button("Отмена");
    private  DateBox dateInput = new DateBox();
    private  TextBox titleInput = new TextBox();
    private  TextBox authorInput = new TextBox();
    private  VerticalPanel verticalPanel = new VerticalPanel();

    public DateBox getDateInput() {
        return dateInput;
    }

    public TextBox getTitleInput() {
        return titleInput;
    }

    public TextBox getAuthorInput() {
        return authorInput;
    }

    protected void initDialog(DialogBox box, Button button) {
        box.setGlassEnabled(true);
        box.setAnimationEnabled(true);
        DefaultDateTimeFormatInfo formatEN =  new DateTimeFormatInfoImpl_en();
        dateInput.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat(formatEN.dateFormatMedium())));
        verticalPanel.add(new HTML("Название: "));
        verticalPanel.add(titleInput);
        verticalPanel.add(new HTML("<p></p>"));
        verticalPanel.add(new HTML("Автор: "));
        verticalPanel.add(authorInput);
        verticalPanel.add(new HTML("<p></p>"));
        verticalPanel.add(new HTML("Дата: "));
        verticalPanel.add(dateInput);
        verticalPanel.add(new HTML("<p></p>"));
        verticalPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        verticalPanel.add(cancelButton);
        verticalPanel.add(button);

        box.add(verticalPanel);

        cancelButton.addClickHandler(event -> box.hide());
    }

    protected abstract void initButton(Button button);

    protected abstract void show();


}
