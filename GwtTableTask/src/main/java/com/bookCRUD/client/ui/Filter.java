package com.bookCRUD.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.bookCRUD.client.EntryPointClass;
import com.bookCRUD.client.objects.BookResult;

import java.util.ArrayList;
import java.util.List;
public class Filter extends Composite {
    private String searchString;
    private List<BookResult> formList;
    @UiField
    VerticalPanel panel = new VerticalPanel();
    private TextBox searchBox = new TextBox();
    private Button searchButton = new Button("Искать!");

    interface FilterUiBinder extends UiBinder<HTMLPanel, Filter> {}

    private static FilterUiBinder ourUiBinder = GWT.create(FilterUiBinder.class);

    public Filter() {
        initWidget(ourUiBinder.createAndBindUi(this));
        panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        panel.add(new HTML("Название или имя автора: "));
        panel.add(searchBox);
        panel.add(searchButton);

        searchButton.addClickHandler(event -> search());
    }

    private void search() {
        searchString = searchBox.getText().toLowerCase();
        if(searchString.equals("")){ EntryPointClass.getProvider().refresh();}
        else { setData(); }
    }


    public void setData() {
        formList = EntryPointClass.getProvider().getList();
        if (formList != null && formList.size() > 0) {
            AsyncDataProvider<BookResult> provider = new AsyncDataProvider<BookResult>() {
                @Override
                protected void onRangeChanged(HasData<BookResult> display) {
                    List<BookResult> sub = getSubList();
                    formList = sub;
                    updateRowData(0, sub);
                }
            };
            provider.addDataDisplay(EntryPointClass.getMyTable().getTable());
            provider.updateRowCount(formList.size(), true);
        }
    }

    private List<BookResult> getSubList() {
        List<BookResult> filtered_list = new ArrayList<>();
            for (BookResult form : formList) {
                if (form.getTitle().toLowerCase().contains(searchString) ||
                        form.getAuthor().toLowerCase().contains(searchString)) {
                    filtered_list.add(form);
                }
            }
        return filtered_list;
    }
}