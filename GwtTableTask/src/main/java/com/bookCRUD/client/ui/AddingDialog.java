package com.bookCRUD.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.bookCRUD.client.EntryPointClass;
import com.bookCRUD.client.RemoteServiceIntf;
import com.bookCRUD.client.objects.Book;
import com.bookCRUD.client.objects.BookResult;
import com.bookCRUD.client.shared.Validator;


public class AddingDialog extends Dialog {
    interface AddingDialogUiBinder extends UiBinder<HTMLPanel, AddingDialog> {}
    private static AddingDialogUiBinder ourUiBinder = GWT.create(AddingDialogUiBinder.class);

    private Button addButton = new Button("Добавить");
    @UiField
    DialogBox dialogBox = new DialogBox(false,true);

    public AddingDialog() {
        initWidget(ourUiBinder.createAndBindUi(this));
        initDialog(dialogBox, addButton);
        initButton(addButton);
    }

    @Override
    protected void initButton(Button button) {
        button.setText("Добавить");
        button.addClickHandler(event -> {
            Book book = new Book(getTitleInput().getText(),getAuthorInput().getText(),getDateInput().getValue());
            if(!(Validator.isBookValid(book))){ return;}
            RemoteServiceIntf.App.getInstance().addBook(book, new AsyncCallback<BookResult>() {
                @Override
                public void onFailure(Throwable caught) {
                    Window.alert(caught.getMessage());
                }
                @Override
                public void onSuccess(BookResult result) {
                    EntryPointClass.getProvider().getList().add(result);
                    dialogBox.hide();
                }
            });
        });
    }
    public void show(){
        dialogBox.center();
        dialogBox.show();
    }
}