package com.bookCRUD.client.ui;

import com.google.gwt.cell.client.*;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.*;
import com.bookCRUD.client.EntryPointClass;
import com.bookCRUD.client.RemoteServiceIntf;
import com.bookCRUD.client.objects.BookResult;

import java.util.ArrayList;
import java.util.List;
public class MyTable extends Composite {
    interface MyTableUiBinder extends UiBinder<HTMLPanel, MyTable> { }
    private static MyTableUiBinder ourUiBinder = GWT.create(MyTableUiBinder.class);
    final static SelectionModel<BookResult> selectionModel = new MultiSelectionModel<>();

    @UiField
    CellTable<BookResult> cellTable;
    @UiField
    Button addButton;
    @UiField
    Button deleteButton;

    public MyTable() {
        initWidget(ourUiBinder.createAndBindUi(this));
        initColumns(cellTable);
        addButton.setText("Добавить");
        deleteButton.setText("Удалить");
        initAddButton(addButton);
        initDeleteButton(deleteButton);
    }

    private void initDeleteButton(Button deleteButton) {
        deleteButton.addClickHandler(event -> {
            List<BookResult> resultList = EntryPointClass.getProvider().getList();
            for (int i = 0; i < resultList.size(); i++) {
                if(cellTable.getVisibleItems().get(i).isChecked()){
                    EntryPointClass.getProvider().getList().remove(resultList.get(i));
                    EntryPointClass.getProvider().refresh();
                    RemoteServiceIntf.App.getInstance().deleteBook(i, new AsyncCallback<BookResult>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            Window.alert(caught.getMessage());
                        }
                        @Override
                        public void onSuccess(BookResult result) { }
                    });
                    i--;
                }
            }
        });
    }

    private void initAddButton(Button button) {
        button.addClickHandler(event -> EntryPointClass.getAddingDialog().show());

    }

    private void initColumns(CellTable<BookResult> cellTable) {
        TextColumn<BookResult> title = new TextColumn<BookResult>() {
            @Override
            public String getValue(BookResult book) {
                return book.getTitle();
            }
        };
        TextColumn<BookResult> author = new TextColumn<BookResult>() {
            @Override
            public String getValue(BookResult book) {
                return book.getAuthor();
            }
        };
        TextColumn<BookResult> date = new TextColumn<BookResult>() {
            @Override
            public String getValue(BookResult book) { return EntryPointClass.getFormatInput().format(book.getDate());
            }
        };

        Column<BookResult, Boolean> checkBoxColumn = new Column<BookResult, Boolean>(new CheckboxCell()) {
            @Override
            public Boolean getValue(BookResult bookResult) {
                bookResult.setChecked(selectionModel.isSelected(bookResult));
                return bookResult.isChecked();
            }
        };
        ButtonCell buttonCell = new ButtonCell();
        Column buttonColumn = new Column<BookResult, String>(buttonCell) {
            @Override
            public String getValue(BookResult object) {
                return "Изменить";
            }
        };

        buttonColumn.setFieldUpdater(new FieldUpdater<BookResult, String>() {
            public void update(int index, BookResult result, String value) {
                EntryPointClass.getChangingDialog().show(result);
            }
        });
        cellTable.addColumn(checkBoxColumn, "");
        cellTable.addColumn(title, "Название");
        cellTable.addColumn(author, "Автор");
        cellTable.addColumn(date, "Дата");
        cellTable.addColumn(buttonColumn, "Button");

        cellTable.setSelectionModel(selectionModel, DefaultSelectionEventManager.<BookResult> createCheckboxManager());

        RemoteServiceIntf.App.getInstance().initBooks(new AsyncCallback<ArrayList<BookResult>>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert(caught.getMessage());
            }

            @Override
            public void onSuccess(ArrayList<BookResult> result) {
                EntryPointClass.getProvider().getList().addAll(result);
            }
        });
    }
    public CellTable<BookResult> getTable() { return cellTable; }
}