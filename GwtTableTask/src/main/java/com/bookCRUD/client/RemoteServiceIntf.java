package com.bookCRUD.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.bookCRUD.client.objects.Book;
import com.bookCRUD.client.objects.BookResult;

import java.util.ArrayList;

@RemoteServiceRelativePath("RemoteServiceIntf")
public interface RemoteServiceIntf extends RemoteService {
    BookResult addBook(Book book);
    BookResult changeBook(BookResult bookResult);
    BookResult deleteBook(int id);
    ArrayList<BookResult> initBooks();
    /**
     * Utility/Convenience class.
     * Use MySampleApplicationService.App.getInstance() to access static instance of RemoteServiceIntfAsync
     */
    public static class App {
        private static RemoteServiceIntfAsync ourInstance = GWT.create(RemoteServiceIntf.class);

        public static synchronized RemoteServiceIntfAsync getInstance() {
            return ourInstance;
        }
    }
}
