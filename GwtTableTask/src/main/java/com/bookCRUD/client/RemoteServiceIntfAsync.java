package com.bookCRUD.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.bookCRUD.client.objects.Book;
import com.bookCRUD.client.objects.BookResult;

import java.util.ArrayList;

public interface RemoteServiceIntfAsync {

    void addBook(Book book, AsyncCallback<BookResult> async);

    void changeBook(BookResult bookResult, AsyncCallback<BookResult> async);

    void deleteBook(int id, AsyncCallback<BookResult> async);

    void initBooks(AsyncCallback<ArrayList<BookResult>> async);
}
