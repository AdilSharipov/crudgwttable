package com.bookCRUD.client.shared;

import com.google.gwt.user.client.Window;
import com.bookCRUD.client.EntryPointClass;
import com.bookCRUD.client.objects.Book;
import com.bookCRUD.client.objects.BookResult;


import java.util.Date;

public class Validator {
    private Validator(){}

    public static boolean isBookValid(Book book){
        String title = book.getTitle();
        String author = book.getAuthor();
        Date date = book.getDate();
        BookResult result = new BookResult(title,author,date);

        boolean isValid1 = !(title == null || title.length() == 0 ||
                author == null || author.length() == 0 ||
                date == null || EntryPointClass.getProvider().getList().contains(result));

        if(!isValid1) {
            Window.alert("Введены пустые поля или такая книга уже есть!");
            return false;
        }

        boolean isValid2 = author.matches("^[^0-9]*$");

        if(!isValid2) {
            Window.alert("Имя введено неверно!");
            return false;
        }

        boolean isValid3 = author.length() <= 80 && title.length() <= 127;

        if(!isValid3) {
            Window.alert("Слишком длинное название или имя автора!");
            return false;
        }

        return true;
    }
}
