package com.bookCRUD.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.ListDataProvider;
import com.bookCRUD.client.objects.BookResult;
import com.bookCRUD.client.ui.AddingDialog;
import com.bookCRUD.client.ui.MyTable;
import com.bookCRUD.client.ui.ChangingDialog;
import com.bookCRUD.client.ui.Filter;

public class EntryPointClass implements EntryPoint {
    private static final ListDataProvider<BookResult> provider = new ListDataProvider<>();
    private static final  DateTimeFormat formatInput = DateTimeFormat.getFormat("dd.LL.yyyy");
    private static final AddingDialog addingDialogPanel = new AddingDialog();
    private static final ChangingDialog changingDialogPanel = new ChangingDialog();
    private static final Filter filter = new Filter();
    private static MyTable table = new MyTable();

    public void onModuleLoad() {
        provider.addDataDisplay(EntryPointClass.getMyTable().getTable());
        RootPanel.get("mainContent").add(table);
        RootPanel.get("filter").add(filter);
    }

    public static DateTimeFormat getFormatInput() {
        return formatInput;
    }

    public static AddingDialog getAddingDialog() {return addingDialogPanel;}

    public static ChangingDialog getChangingDialog() { return changingDialogPanel; }

    public static MyTable getMyTable() { return table; }

    public static ListDataProvider<BookResult> getProvider() { return provider; }
}
