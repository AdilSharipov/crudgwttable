package com.bookCRUD.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.bookCRUD.client.RemoteServiceIntf;
import com.bookCRUD.client.objects.Book;
import com.bookCRUD.client.objects.BookResult;

import java.util.ArrayList;
import java.util.Date;

public class RemoteServiceImpl extends RemoteServiceServlet implements RemoteServiceIntf {
    private static Book[] booksArr = {
            new Book("Три мушкетёра", "Александр Дюма", new Date()),
            new Book("Три мушкетёра. 20 лет спустя", "Александр Дюма", new Date()),
            new Book("Три мушкетёра. 10 лет спустя", "Александр Дюма", new Date()),
            new Book("Сказка о царе Салтане, о сыне его славном и могучем богатыре князе " +
                    "Гвидоне Салтановиче и о прекрасной царевне Лебеди", "Александр Сергеевич Пушкин", new Date()),
            new Book("Морской Волк", "Джек Лондон", new Date()),
    };
    @Override
    public BookResult addBook(Book book) {
        Library.addBook(book);
        BookResult bookResult = new BookResult(Library.getBooksSize()-1,book.getTitle(),book.getAuthor(),book.getDate());
        return bookResult;
    }

    @Override
    public BookResult changeBook(BookResult bookResult) {
        Book book = new Book(bookResult.getTitle(),bookResult.getAuthor(),bookResult.getDate());
        Library.updateBook(bookResult.getId(), book);
        return bookResult;
    }

    @Override
    public BookResult deleteBook(int id) {
        Book book = Library.getBook(id);
        BookResult bookresult = new BookResult(id,book.getTitle(),book.getAuthor(),book.getDate());
        Library.deleteBook(id);
        return bookresult;
    }
    @Override
    public ArrayList<BookResult> initBooks(){
        Library.initBooks(booksArr);
        ArrayList<BookResult> results = new ArrayList<>();
        for (int i = 0; i < booksArr.length; i++) {
            results.add(new BookResult(booksArr[i].getTitle(),booksArr[i].getAuthor(),booksArr[i].getDate()));
        }
        return results;
    }
}